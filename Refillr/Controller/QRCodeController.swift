//
//  QRCodeController.swift
//  Refillr
//
//  Created by Sandesh Shetty on 11/2/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

class QRCodeController: UIViewController, UINavigationControllerDelegate {
    
    /// Firebase vision instance.
    // [START init_vision]
    lazy var vision = Vision.vision()
    // [END init_vision]
    
    /// A string holding current results from detection.
    var resultsText = ""
    
    private lazy var annotationOverlayView: UIView = {
       precondition(isViewLoaded)
        let annotationOVerlayView = UIView(frame: .zero)
        annotationOVerlayView.translatesAutoresizingMaskIntoConstraints = false
        return annotationOVerlayView
    }()
    
    // An image picker for accessing the captured image
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imageView: UIImageView!
    
    enum error: Error {
        case noCamerAvailable
        case videoInputInitFail
    }
    
    override func viewDidLoad() {
        imageView.image = UIImage(named: "bear_first")
        imageView.addSubview(annotationOverlayView)
        NSLayoutConstraint.activate([
            annotationOverlayView.topAnchor.constraint(equalTo: imageView.topAnchor),
            annotationOverlayView.leadingAnchor.constraint(equalTo: imageView.leadingAnchor),
            annotationOverlayView.trailingAnchor.constraint(equalTo: imageView.trailingAnchor),
            annotationOverlayView.bottomAnchor.constraint(equalTo: imageView.bottomAnchor)
            ])
        
        imagePicker.delegate = self
        imagePicker.sourceType = .camera
    
    }

    
    @IBAction func openCamera(_ sender: UIButton) {
        present(imagePicker,animated: true)
    }
    
    /// Updates the image view with a scaled version of the given image.
    private func updateImageView(with image: UIImage) {
        let orientation = UIApplication.shared.statusBarOrientation
        var scaledImageWidth: CGFloat = 0.0
        var scaledImageHeight: CGFloat = 0.0
        switch orientation {
        case .portrait, .portraitUpsideDown, .unknown:
            scaledImageWidth = imageView.bounds.size.width
            scaledImageHeight = image.size.height * scaledImageWidth / image.size.width
        case .landscapeLeft, .landscapeRight:
            scaledImageWidth = image.size.width * scaledImageHeight / image.size.height
            scaledImageHeight = imageView.bounds.size.height
        }
        DispatchQueue.global(qos: .userInitiated).async {
            // Scale image while maintaining aspect ratio so it displays better in the UIImageView.
            
            
//            var scaledImage = image.scaledImage(
//                withSize: CGSize(width: scaledImageWidth, height: scaledImageHeight)
//            )
//            scaledImage = scaledImage ?? image
           
            UIGraphicsBeginImageContextWithOptions(CGSize(width: scaledImageWidth, height: scaledImageHeight), false, 0.0)
            image.draw(in: CGRect(x: 0, y: 0, width: scaledImageWidth, height: scaledImageHeight))
            let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
         
            
            guard let finalImage = scaledImage else { return }
            DispatchQueue.main.async {
                self.imageView.image = finalImage
            }
            self.detectBarcodes(image: self.imageView.image)
        }
    }
    
    // Detects barcodes on the specified image and draws a frame around the detected barcodes using
    /// On-Device barcode API.
    ///
    /// - Parameter image: The image.
    func detectBarcodes(image: UIImage?) {
        guard let image = image else { return }
        
        // Define the options for a barcode detector.
        // [START config_barcode]
        let format = VisionBarcodeFormat.all
        let barcodeOptions = VisionBarcodeDetectorOptions(formats: format)
        // [END config_barcode]
        // Create a barcode detector.
        // [START init_barcode]
        let barcodeDetector = vision.barcodeDetector(options: barcodeOptions)
        // [END init_barcode]
        // Define the metadata for the image.
        let imageMetadata = VisionImageMetadata()
        imageMetadata.orientation = self.visionImageOrientation(from: image.imageOrientation)
        
        // Initialize a VisionImage object with the given UIImage.
        let visionImage = VisionImage(image: image)
        visionImage.metadata = imageMetadata
        
        // [START detect_barcodes]
        barcodeDetector.detect(in: visionImage) { features, error in
            guard error == nil, let features = features, !features.isEmpty else {
                // [START_EXCLUDE]
                let errorString = error?.localizedDescription
                self.resultsText = "On-Device barcode detection failed with error: \(errorString)"
                self.showResults()
                // [END_EXCLUDE]
                return
            }
            
            // [START_EXCLUDE]
            self.resultsText = features.map { feature in
//                let transformedRect = feature.frame.applying(self.transformMatrix())
//                UIUtilities.addRectangle(
//                    transformedRect,
//                    to: self.annotationOverlayView,
//                    color: UIColor.green
//                )
                return "DisplayValue: \(feature.displayValue ?? ""), RawValue: " +
                "\(feature.rawValue ?? ""), Frame: \(feature.frame)"
                }.joined(separator: "\n")
            self.showResults()
            // [END_EXCLUDE]
        }
        // [END detect_barcodes]
        
     
    }
    
    private func showResults() {
        let resultsAlertController = UIAlertController(
            title: "Detection Results",
            message: nil,
            preferredStyle: .actionSheet
        )
        resultsAlertController.addAction(
            UIAlertAction(title: "OK", style: .destructive) { _ in
                resultsAlertController.dismiss(animated: true, completion: nil)
            }
        )
        resultsAlertController.message = resultsText
        //resultsAlertController.popoverPresentationController?.barButtonItem = detectButton
        resultsAlertController.popoverPresentationController?.sourceView = self.view
        present(resultsAlertController, animated: true, completion: nil)
        print(resultsText)
    }
    
    public func visionImageOrientation(
        from imageOrientation: UIImage.Orientation
        ) -> VisionDetectorImageOrientation {
        switch imageOrientation {
        case .up:
            return .topLeft
        case .down:
            return .bottomRight
        case .left:
            return .leftBottom
        case .right:
            return .rightTop
        case .upMirrored:
            return .topRight
        case .downMirrored:
            return .bottomLeft
        case .leftMirrored:
            return .leftTop
        case .rightMirrored:
            return .rightBottom
        }
    }
    
    public  func imageOrientation(
        fromDevicePosition devicePosition: AVCaptureDevice.Position = .back
        ) -> UIImage.Orientation {
        var deviceOrientation = UIDevice.current.orientation
        if deviceOrientation == .faceDown || deviceOrientation == .faceUp ||
            deviceOrientation == .unknown {
            deviceOrientation = currentUIOrientation()
        }
        switch deviceOrientation {
        case .portrait:
            return devicePosition == .front ? .leftMirrored : .right
        case .landscapeLeft:
            return devicePosition == .front ? .downMirrored : .up
        case .portraitUpsideDown:
            return devicePosition == .front ? .rightMirrored : .left
        case .landscapeRight:
            return devicePosition == .front ? .upMirrored : .down
        case .faceDown, .faceUp, .unknown:
            return .up
        }
    }
    
    private  func currentUIOrientation() -> UIDeviceOrientation {
        let deviceOrientation = { () -> UIDeviceOrientation in
            switch UIApplication.shared.statusBarOrientation {
            case .landscapeLeft:
                return .landscapeRight
            case .landscapeRight:
                return .landscapeLeft
            case .portraitUpsideDown:
                return .portraitUpsideDown
            case .portrait, .unknown:
                return .portrait
            }
        }
        guard Thread.isMainThread else {
            var currentOrientation: UIDeviceOrientation = .portrait
            DispatchQueue.main.sync {
                currentOrientation = deviceOrientation()
            }
            return currentOrientation
        }
        return deviceOrientation()
    }
}

extension QRCodeController: UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
           updateImageView(with: pickedImage)
        }
        dismiss(animated: true)
    }
    
}
