//
//  ProfileController.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/24/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit
import Firebase

class ProfileController: UIViewController
{
    
    override func viewDidLoad() {
        view.backgroundColor = UIColor.red
    }
    
    @IBAction func logoutPressed(_ sender: UIButton) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            
           // self.navigationController?.popToRootViewController(animated: true)
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
}
