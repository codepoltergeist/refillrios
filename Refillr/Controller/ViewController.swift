//
//  ViewController.swift // Login Controller
//  Refillr
//
//  Created by Sandesh Shetty on 8/17/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit
import Firebase

//extension UIColor {
//    static var mainPink1 = UIColor(red: 232/255, green: 68/255, blue: 133/255, alpha: 1)
//}

class ViewController: UIViewController {

   
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if Auth.auth().currentUser != nil {
            print("User already Logged in")
            self.performSegue(withIdentifier: "afterLogin", sender: nil)
        }else {
            print("No User Logged in")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        Auth.auth().signIn(withEmail: email.text!, password: password.text!) {
            (user, error) in
            
            if error != nil {
                print("LoginError:\(error?.localizedDescription ?? "error")")
            }else{
                print("Login Successfull)")
                self.performSegue(withIdentifier: "afterLogin", sender: self)
            }
            
        }
    }
    
    @IBAction func registerPressed(_ sender: UIButton) {
        performSegue(withIdentifier: "goToRegister", sender: self)
    }
}
