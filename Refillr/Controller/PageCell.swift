//
//  PageCell.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/21/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit

class PageCell: UICollectionViewCell
{
    var page: Page? {
        didSet {
            
            guard let unwrappedPage = page else {
                return
            }
            
            bearImageView.image = UIImage(named: unwrappedPage.imageName)
            let attributedText = NSMutableAttributedString(string: unwrappedPage.headerText, attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)])
            
            attributedText.append(NSAttributedString(string: "\n\n\nAre you ready for loads and loads of fun? Don't wait any longer! We hope to see you in our stores soon.", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.gray]))
            
            
            descriptionText.attributedText = attributedText
            descriptionText.textAlignment = .center
            
        }
    }
    
    // {} for closures or anonmyous func
    private let bearImageView : UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "bear_first"))
        //enables auto layout in view
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let descriptionText: UITextView = {
        let description = UITextView()
        
        let attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 18)])
        
        attributedText.append(NSAttributedString(string: "", attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13), NSAttributedString.Key.foregroundColor: UIColor.gray]))
        
        description.attributedText = attributedText
        
        //        description.text = "Join us today in our fun and games!"
        //        description.font = UIFont.boldSystemFont(ofSize: 18)
        
        description.textAlignment = .center
        description.translatesAutoresizingMaskIntoConstraints = false
        description.isEditable = false
        description.isScrollEnabled = false
        
        return description
    }()
    
   
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        addSubview(descriptionText)
        
        setUpLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
    private func setUpLayout() {
        
        let topImageContainerView = UIView()
        addSubview(topImageContainerView)
        
        // to enable auto layout
        topImageContainerView.translatesAutoresizingMaskIntoConstraints = false
        topImageContainerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        topImageContainerView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        topImageContainerView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        topImageContainerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5).isActive = true
        
        topImageContainerView.addSubview(bearImageView)
        
        bearImageView.centerXAnchor.constraint(equalTo: topImageContainerView.centerXAnchor).isActive = true
        bearImageView.centerYAnchor.constraint(equalTo: topImageContainerView.centerYAnchor).isActive = true
        //        bearImageView.topAnchor.constraint(equalTo: topImageContainerView.topAnchor, constant: 100).isActive = true
        bearImageView.heightAnchor.constraint(equalTo: topImageContainerView.heightAnchor, multiplier: 0.5).isActive = true
        bearImageView.widthAnchor.constraint(equalTo: topImageContainerView.widthAnchor, multiplier: 0.5).isActive = true
        
        
        descriptionText.topAnchor.constraint(equalTo: topImageContainerView.bottomAnchor).isActive = true
        descriptionText.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24).isActive = true
        descriptionText.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24).isActive = true
        descriptionText.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
    }
    
}
