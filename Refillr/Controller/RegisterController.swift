//
//  RegisterController.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/24/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit
import Firebase

class RegisterController: UIViewController {

    
    @IBOutlet weak var displayName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    
    var reference: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        reference = Database.database().reference()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func registerUser(_ sender: UIButton) {
        Auth.auth().createUser(withEmail: email.text!, password: password.text!) {
            (user, error) in
            
            if error != nil {
                print("Error while registering User")
            }else {
                guard let currentUser = user?.user else { return }
                
                let user = User(displayName: self.displayName.text!, email: currentUser.email!, phoneNumber: self.phoneNumber.text!, providerId: currentUser.providerID, uid: currentUser.uid)
                
                self.addUserToDatabase(user: user)
            }
            
        }
    }
    
    func addUserToDatabase(user: User)  {
        self.reference.child("user").child(user.uid).setValue(user.toAnyObject()){
            (error: Error?, ref:DatabaseReference) in
            if let error = error {
                print("Data could not be saved: \(error).")
            } else {
                print("Data saved successfully!")
                self.performSegue(withIdentifier: "afterRegister", sender: self)
            }
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
