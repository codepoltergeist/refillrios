//
//  Page.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/23/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit

struct Page {
    let imageName: String
    let headerText: String
}
