//
//  User.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/24/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit
import Firebase

struct User{
    
    
    var uid: String!
    var email: String!
    var displayName: String!
    var phoneNumber: String!
    var providerId: String!
    
    // Grab user data from Firebase and set the object?
    init( displayName: String, email: String,phoneNumber: String,providerId: String,uid: String) {
        self.uid = uid
        self.email = email
        self.displayName = displayName
        self.phoneNumber = phoneNumber
        self.providerId = providerId
        
    }
    // Init for reading from Database snapshot
    init(snapshot: DataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        uid = (snapshotValue["uid"] as! String)
        email = (snapshotValue["email"] as! String)
        displayName = (snapshotValue["displayName"] as! String)
        phoneNumber = (snapshotValue["phoneNumber"] as! String)
        providerId = (snapshotValue["providerId"] as! String)
    }
    
    // Func converting model for easier writing to database
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "email": email,
            "displayName": displayName,
            "phoneNumber": phoneNumber,
            "providerId": providerId
        ]
    }
    
    
}
