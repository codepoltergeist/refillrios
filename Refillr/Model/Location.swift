//
//  Location.swift
//  Refillr
//
//  Created by Sandesh Shetty on 10/25/18.
//  Copyright © 2018 Sandesh Shetty. All rights reserved.
//

import UIKit

struct Location {
    
    var id: String
    var latitude: String
    var longitude: String
    var name: String
    var minpoints: String
    var points: String
    
    init(id: String, latitude: String, longitude: String, name: String, minpoints: String, points: String) {
        self.id = id
        self.latitude = latitude
        self.longitude = longitude
        self.name = name
        self.minpoints = minpoints
        self.points = points
    }
    
}
